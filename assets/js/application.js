!(function($){
    var dp = $('.date')
    ,   df = $('.date-filter')
    ,   popover = $('.pop')
    ,   print = $('.print')
    ,   dd = $('#user_id')
    ,	status = $('#status')
    ,	d = new Date();
    
    dp.datepicker({
    	format: "dd-mm-yyyy",
    	language: 'es',
    	autoclose: true,
    	startDate: dp.val()
    });

    df.datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        autoclose: true
    });

    print.click(function(event) {
        $(this).closest('form').attr('action','/tickets/print_list');
    });

    popover.popover({trigger: 'hover', html: 'true', container: 'body'});

    dd.select2({width:"250px"});
    status.select2({width:"200px"});

})(jQuery);