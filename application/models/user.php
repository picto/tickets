<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User model
 *
 * @package     Models
 * @author      rcasares
 */

class User extends DataMapper {

	// Insert related models that User can have just one of.
	var $has_one = array('group');

	// Insert related models that User can have more than one of.
	var $has_many = array(
        'ticket'
    );

    var $validation = array(
       'name' => array(
            'label' => 'nombre',
            'rules' => array('required')
        ),
       'lastname' => array(
            'label' => 'apellido',
            'rules' => array('required')
        ),
       'password' => array(
            'label' => 'contraseña',
            'rules' => array('min_length' => 4,'matches' => 'password_confirm', 'encrypt')
        ),
       'password_confirm' => array(
            'label' => 'confirmar contraseña',
            'rules' => array('encrypt')
        ),
       'email' => array(
            'label' => 'dirección de correo',
            'rules' => array('required','valid_email','unique')
        )
    );

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

    /**
     * login checks user login data
     *
     * @author rcasares
     **/
    function login()
    {
        $email = $this->email;

        $u = new User();

        $u->where('email', $email)->get();

        $this->hash = $u->hash;

        $this->validate()->get();
        if ($this->exists())
        {
            return TRUE;
        }
        else
        {
            $this->error_message('login', 'Credenciales incorrectas');
            $this->email = $email;
            return FALSE;
        }
    }

    /**
     * _encrypt encrypts user passwords using a random hash
     *
     * @author rcasares
     **/
    function _encrypt($field)
    {
        // Don't encrypt an empty string
        if (!empty($this->{$field}))
        {
            // Generate a random hash if empty
            if (empty($this->hash))
            {
                $this->hash = md5(uniqid(rand(), true));
            }

            $this->{$field} = sha1($this->hash . $this->{$field});
        }
    }

}

/* End of file user.php */
/* Location: ./application/models/user.php */