<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ticket model
 *
 * @package     Models
 * @author      rcasares
 */

class Ticket extends DataMapper {

	// Insert related models that Payment can have more than one of.
	var $has_one = array('user');

	var $validation = array(
       'date' => array(
            'label' => 'fecha',
            'rules' => array('required','valid_date','formatDate')
        )
    );

    function _formatDate($field) {
    	$this->{$field} = DateTime::createFromFormat('d-m-Y',$this->{$field})->format('Y-m-d');        
    }

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

}

/* End of file ticket.php */
/* Location: ./application/models/ticket.php */