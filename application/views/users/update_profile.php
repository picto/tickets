<div class="row">
    <div class="span4 offset4">
      <div class="bordered">
        <form method="post">
            <fieldset>
                <legend>Actualizar información</legend>
                <?php if($admin): ?>
                    <div class="control-group <?php echo hasError($user->error->group_id) ?>">
                        <label class="control-label" for="email">Grupo</label>
                        <div class="controls">
                            <?php echo form_dropdown('group_id', $groups, $user->group_id); ?>
                            <?php echo form_help($user->error->group_id) ?>
                        </div>
                    </div>
                <?php endif ?>
                <div class="control-group <?php echo hasError($user->error->email) ?>">
                    <label class="control-label" for="email">Dirección de correo</label>
                    <div class="controls">
                        <input id="email" name="email" class="input-block-level" type="text" value="<?php echo $user->email ?>">
                        <?php echo form_help($user->error->email) ?>
                    </div>
                </div>
                <div class="control-group <?php echo hasError($user->error->name) ?>">
                    <label class="control-label" for="name">Nombre</label>
                    <div class="controls">
                        <input id="name" name="name" class="input-block-level" type="text" value="<?php echo $user->name ?>">
                        <?php echo form_help($user->error->name) ?>
                    </div>
                </div>
                <div class="control-group <?php echo hasError($user->error->lastname) ?>">
                    <label class="control-label" for="lastname">Apellido</label>
                    <div class="controls">
                        <input id="lastname" name="lastname" class="input-block-level" type="text" value="<?php echo $user->lastname ?>">
                        <?php echo form_help($user->error->lastname) ?>
                    </div>
                </div>
                <div class="control-group <?php echo hasError($user->error->phone) ?>" value="<?php echo $user->phone ?>">
                    <label class="control-label" for="phone">Teléfono</label>
                    <div class="controls">
                        <input id="phone" name="phone" class="input-block-level" type="text" value="<?php echo $user->phone ?>">
                        <?php echo form_help($user->error->phone) ?>
                    </div>
                </div>
                <button type="submit" class="btn btn-success"><i class="icon-user"></i> Actualizar mis datos</button>
                <a href="<?php echo $this->agent->referrer() ?>" class="btn">Volver</a>
            </fieldset>
            <?php if($admin): ?>
                <input type="hidden" name="id" value="<?php echo $user->id ?>">
            <?php endif ?>
        </form>
      </div>
    </div>
</div>