<div class="row">
    <div class="span12">
        <div class="bordered">
            <h2 class="page-header"><i class="icon-group"></i> Usuarios</h2>
            <?php if($users->exists()): ?>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Apellido</th>
                        <th>Nombre</th>
                        <th>Grupo</th>
                        <th>Email</th>
                        <th>Teléfono</th>
                        <th>Acciones</th>
                    </tr>
                <?php foreach($users as $u): ?>
                    <tr>
                        <td><?php echo $u->lastname ?></td>
                        <td><?php echo $u->name ?></td>
                        <td><?php echo $u->group_name ?></td>
                        <td><?php echo $u->email ?></td>
                        <td><?php echo $u->phone ?></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo site_url("users/update_profile/$u->id") ?>" class="btn btn-mini"><i class="fa fa-edit"></i> Actualizar</a>
                                <a href="<?php echo site_url("users/delete/$u->id") ?>" class="btn btn-danger btn-mini"><i class="fa fa-trash-o"></i> Eliminar</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach?>
                </table>
                <div class="pagination pagination-centered">
                    <ul>
                        <li class="<?php echo ($page > 1) ? '' : 'disabled' ?>">
                            <a href="<?php echo ($page > 1) ? site_url('users/page/'.($page-1)) : '#' ?>">Anterior</a>
                        </li>
                        <li class="disabled">
                            <a href="#"><?php echo $page." de ".ceil($total/$offset) ?></a>
                        </li>
                        <li class="<?php echo (($total/$offset) > $page) ? '' : 'disabled' ?>">
                            <a href="<?php echo (($total/$offset) > $page) ? site_url('users/page/'.($page+1)) : '#' ?>">Siguiente</a>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="alert alert-info">
                    <i class="icon-info-sign"></i>
                    No hay usuarios registrados.
                </div>
            <?php endif?>
        </div>
    </div>
</div>