<div class="row">
    <div class="span4 offset4">
      <div class="bordered">
        <form method="post" action="<?php echo site_url('users/change_password')?>">
            <fieldset>
                <legend>Cambia tu contraseña</legend>
                <div class="control-group <?php echo hasError($user->error->login) ?>">
                    <label class="control-label" for="current_password">Contraseña actual</label>
                    <div class="controls">
                        <input id="current_password" name="current_password" class="input-block-level" type="password">
                        <?php echo form_help($user->error->login) ?>
                    </div>
                </div>
                <div class="control-group <?php echo hasError($user->error->password) ?>">
                    <label class="control-label" for="password">Nueva contraseña</label>
                    <div class="controls">
                        <input id="password" name="password" class="input-block-level" type="password">
                        <?php echo form_help($user->error->password) ?>
                    </div>
                </div>
                <div class="control-group <?php echo hasError($user->error->password_confirm) ?>">
                    <label class="control-label" for="password_confirm">Confirmar nueva contraseña</label>
                    <div class="controls">
                        <input id="password_confirm" name="password_confirm" class="input-block-level" type="password">
                        <?php echo form_help($user->error->password_confirm) ?>
                    </div>
                </div>
                <button type="submit" class="btn btn-success"><i class="icon-signin"></i> Cambiar mi contraseña</button>
                <a href="<?php echo $this->agent->referrer() ?>" class="btn">Cancelar</a>
            </fieldset>
        </form>
      </div>
    </div>
</div>