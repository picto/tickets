<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sistema de tickets</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url("/assets/css/application.min.css")?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>
    <div class="banner hidden-phone hidden-tablet">
      
        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8- »
      444553540000" codebase="http://fpdownload.macromedia.com »
      /pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" »
      width="430" height="100">
      <param name="movie" value="/assets/swf/ischigualasto.swf" />
      <param name="wmode" value="transparent">
      <embed type="application/x-shockwave-flash" »
      src="/assets/swf/ischigualasto.swf" wmode="transparent" width="430" height="100" »
      pluginspage="http://www.macromedia.com/go »
      /getflashplayer" />
      </object>
      
    </div>
    <div class="navbar navbar-isc navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <?php if($connected): ?>
                <?php if ($admin): ?>
                    <li <?php active($this->uri->uri_string(),'tickets/payments') ?>><a href="<?php echo site_url('tickets/payments') ?>"><i class="fa fa-credit-card"></i> Pagos</a></li>
                    <li <?php active($this->uri->uri_string(),'users') ?>><a href="<?php echo site_url('users') ?>"><i class="fa fa-users"></i> Usuarios</a></li>
                <?php endif ?>
                <?php if (!$admin): ?>
                  <li <?php active($this->uri->uri_string(),'tickets/buy') ?>><a href="<?php echo site_url('tickets/buy') ?>"><i class="fa fa-credit-card"></i> Comprar</a></li>
                  <li <?php active($this->uri->uri_string(),'tickets') ?>><a href="<?php echo site_url('tickets') ?>"><i class="fa fa-ticket"></i> Mis tickets</a></li>
                <?php endif ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Mi perfil <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li <?php active($this->uri->uri_string(),'users/update_profile') ?>><a href="<?php echo site_url('users/update_profile') ?>">Actualizar mis datos</a></li>
                        <li <?php active($this->uri->uri_string(),'users/change_password') ?>><a href="<?php echo site_url('users/change_password') ?>">Cambiar contraseña</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('logout') ?>"><i class="fa fa-unlock"></i> Cerrar sesión</a></li>
                    </ul>
                </li>
              <?php else: ?>
                <li <?php active($this->uri->segment(1),'') ?>><a href="<?php echo site_url('/') ?>"><i class="fa fa-home"></i> Inicio</a></li>
                <li <?php active($this->uri->segment(1),'signup') ?>><a href="<?php echo site_url('signup') ?>"><i class="fa fa-user"></i> Crear una cuenta</a></li>
                <li <?php active($this->uri->segment(1),'login') ?>><a href="<?php echo site_url('login') ?>"><i class="fa fa-lock"></i> Iniciar sesión</a></li>
              <?php endif ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
        <?php if($this->session->flashdata('alert')):?>
        <?php   extract($this->session->flashdata('alert'))?>
        <div class="alert alert-<?php echo $type?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php   echo $msg?>
        </div>
        <?php endif?>

        <?php echo $yield ?>

    </div><!-- /container -->
    <div class="container text-center">
      <img src="/assets/img/parque.png">
      <img src="/assets/img/gobierno.png">
      <a href="http://qr.afip.gob.ar/?qr=J8JyvypY2hTeeiP9SPbuPg,,">
        <img src="/assets/img/dataweb.jpg" width="50px">
      </a>
    </div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
    <script src="<?php echo base_url("/assets/js/application.min.js")?>"></script>
  </body>
</html>