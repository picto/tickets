<div class="row">
    <div class="span4 offset4">
      <div class="bordered">
        <form method="post" action="<?php echo site_url('forgot')?>">
          <fieldset>
            <legend>Recuperar contraseña</legend>
            <div class="control-group <?php echo hasError($user->error->email) ?>">
                <label class="control-label" for="email">Dirección de correo</label>
                <div class="controls">
                    <input id="email" name="email" class="input-block-level" type="text" value="<?php echo $user->email ?>">
                    <?php echo form_help($user->error->email) ?>
                </div>
            </div>
            <button type="submit" class="btn btn-success"><i class="fa fa-lock"></i> Recuperar contraseña</button>
            <a href="<?php echo site_url('login') ?>" class="btn">Cancelar</a>
          </fieldset>
        </form>
      </div>
    </div>
</div>