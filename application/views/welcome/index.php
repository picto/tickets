<div class="hero-unit hongo">
	<p class="text-center">
		<img src="assets/img/logo.png">
	</p>
	<br>
	<p class="text-center hero-btn">
		<a href="<?php echo site_url('signup')?>" class="btn btn-large btn-warning red"><i class="fa fa-user"></i> Crear una cuenta</a>
		<a href="<?php echo site_url('login')?>" class="btn btn-large btn-warning orange"><i class="fa fa-lock"></i> Iniciar sesión</a>
	</p>
</div>