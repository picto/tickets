<div class="row">
    <div class="span4 offset4">
      <div class="bordered">
        <form method="post" action="<?php echo site_url('signup')?>">
		  <fieldset>
		    <legend>Crear una cuenta</legend>
            <div class="control-group <?php echo hasError($user->error->email) ?>">
                <label class="control-label" for="email">Dirección de correo</label>
                <div class="controls">
                    <input id="email" name="email" class="input-block-level" type="text" value="<?php echo $user->email ?>">
                    <?php echo form_help($user->error->email) ?>
                </div>
            </div>
            <div class="control-group <?php echo hasError($user->error->password) ?>">
                <label class="control-label" for="password">Contraseña</label>
                <div class="controls">
                    <input id="password" name="password" class="input-block-level" type="password">
                    <?php echo form_help($user->error->password) ?>
                </div>
            </div>
            <div class="control-group <?php echo hasError($user->error->password_confirm) ?>">
                <label class="control-label" for="password_confirm">Confirmar contraseña</label>
                <div class="controls">
                    <input id="password_confirm" name="password_confirm" class="input-block-level" type="password">
                    <?php echo form_help($user->error->password_confirm) ?>
                </div>
            </div>
            <div class="control-group <?php echo hasError($user->error->name) ?>">
                <label class="control-label" for="name">Nombre</label>
                <div class="controls">
                    <input id="name" name="name" class="input-block-level" type="text" value="<?php echo $user->name ?>">
                    <?php echo form_help($user->error->name) ?>
                </div>
            </div>
            <div class="control-group <?php echo hasError($user->error->lastname) ?>">
                <label class="control-label" for="lastname">Apellido</label>
                <div class="controls">
                    <input id="lastname" name="lastname" class="input-block-level" type="text" value="<?php echo $user->lastname ?>">
                    <?php echo form_help($user->error->lastname) ?>
                </div>
            </div>
            <div class="control-group <?php echo hasError($user->error->phone) ?>" value="<?php echo $user->phone ?>">
                <label class="control-label" for="phone">Teléfono</label>
                <div class="controls">
                    <input id="phone" name="phone" class="input-block-level" type="text" value="<?php echo $user->phone ?>">
                    <?php echo form_help($user->error->phone) ?>
                </div>
            </div>
		    <button type="submit" class="btn btn-success"><i class="fa fa-user"></i> Crear mi cuenta</button>
		    <a href="<?php echo $this->agent->referrer() ?>" class="btn">Volver</a>
		  </fieldset>
		</form>
      </div>
    </div>
</div>