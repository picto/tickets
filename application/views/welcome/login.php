<div class="row">
    <div class="span4 offset4">
      <div class="bordered">
        <form method="post" action="<?php echo site_url('welcome/login')?>">
		  <fieldset>
		    <legend>Iniciar sesión</legend>
            <?php if($user->error->login): ?>
            <div class="control-group alert alert-error"><i class="icon-warning-sign"></i> <?php echo $user->error->login ?></div>
            <?php endif ?>
            <div class="control-group <?php echo hasError($user->error->email) ?>">
                <label class="control-label" for="email">Dirección de correo</label>
                <div class="controls">
                    <input id="email" name="email" class="input-block-level" type="text" value="<?php echo $user->email ?>">
                    <?php echo form_help($user->error->email) ?>
                </div>
            </div>
		    <div class="control-group">
                <label class="control-label" for="password">Contraseña</label>
                <div class="controls">
                    <input id="password" name="password" class="input-block-level" type="password">
                </div>
            </div>
            <button type="submit" class="btn btn-success"><i class="fa fa-lock"></i> Iniciar sesión</button>
		    <hr>
		    <p><a href="<?php echo site_url('forgot') ?>">¿Perdió su contraseña?</a></p>
		  </fieldset>
		</form>
      </div>
    </div>
</div>