<html>
<head>
	<title>Listado de tickets</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
</head>
<body>

</body>
</html>
<div class="container">
	<div class="row">
	    <div class="span12">
	        <div class="bordered">
	            <h2 class="page-header"><i class="fa fa-ticket"></i> Listado completo <?php echo $this->input->post('date') ?></h2>
	            <?php if($tickets->exists()): ?>
	                <table class="table table-bordered table-striped">
	                    <tr>
	                        <th>Usuario</th>
	                        <th>Monto</th>
	                        <th>Fecha evento</th>
	                        <th>Creado</th>
	                        <th>Estado</th>
	                    </tr>
	                <?php foreach($tickets as $t): ?>
	                    <tr>
	                        <td>
	                        	<?php echo $t->user_name ?> 
	                        	<?php echo $t->user_lastname ?>
	                        </td>
	                        <td>$ <?php echo number_format($t->amount,2,',','.') ?></td>
	                        <td><?php echo date_format(date_create($t->date),'d/m/Y') ?></td>
	                        <td><?php echo date_format(date_create($t->created),'d/m/Y h:i') ?></td>
	                        <td><?php echo status($t->status) ?></td>
	                    </tr>
	                <?php endforeach?>
	                </table>
	            <?php else: ?>
	                <div class="alert alert-info">
	                    <i class="icon-info-sign"></i>
	                    No hay tickets registrados.
	                </div>
	            <?php endif?>
	        </div>
	    </div>
	</div>
</div>