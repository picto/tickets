<div class="row">
    <div class="span12">
        <div class="bordered">
            <h2 class="page-header"><i class="fa fa-ticket"></i> Tickets</h2>
            <?php if($tickets->exists()): ?>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Monto</th>
                        <th>Fecha evento</th>
                        <th>Creado</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                <?php foreach($tickets as $t): ?>
                    <tr>
                        <td>$ <?php echo number_format($t->amount,2,',','.') ?></td>
                        <td><?php echo date_format(date_create($t->date),'d/m/Y') ?></td>
                        <td><?php echo date_format(date_create($t->created),'d/m/Y h:i') ?></td>
                        <td><?php echo status($t->status) ?></td>
                        <td>
                            <div class="btn-group">
                                <?php echo print_btn($t); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach?>
                </table>
                <div class="pagination pagination-centered">
                    <ul>
                        <li class="<?php echo ($page > 1) ? '' : 'disabled' ?>">
                            <a href="<?php echo ($page > 1) ? site_url('tickets/page/'.($page-1)) : '#' ?>">Anterior</a>
                        </li>
                        <li class="disabled">
                            <a href="#"><?php echo $page." de ".ceil($total/$offset) ?></a>
                        </li>
                        <li class="<?php echo (($total/$offset) > $page) ? '' : 'disabled' ?>">
                            <a href="<?php echo (($total/$offset) > $page) ? site_url('tickets/page/'.($page+1)) : '#' ?>">Siguiente</a>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="alert alert-info">
                    <i class="icon-info-sign"></i>
                    No hay tickets registrados.
                </div>
            <?php endif?>
        </div>
    </div>
</div>