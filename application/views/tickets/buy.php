<div class="row">
    <div class="span4 offset4">
        <div class="bordered">
            <h2 class="page-header"><i class="fa fa-credit-card"></i> Seleccionar fecha para el evento</h2>
            <form method="post" action="<?php echo site_url('tickets/buy')?>">
			  	<fieldset>
                <div class="control-group <?php echo hasError($ticket->error->amount) ?>">
                    <label class="control-label" for="amount">
                        Tarifa
                    </label>
                    <div class="controls">
                        <select id="amount" name="amount">
                            <optgroup label="Circuito Clásico">
                                <option value="160">Extranjeros $160</option>
                                <option value="100">Residentes de Argentina $100</option>
                                <option value="60">Residentes de San Juan $60</option>
                                <option value="70">Jubilados de Argentina $70</option>
                                <option value="50">Jubilados de San Juan $50</option>
                                <option value="70">Estudiantes de Argentina $70</option>
                                <option value="50">Estudiantes de San Juan $50</option>
                            </optgroup>
                            <optgroup label="Circuitos Alternativos">
                                <option value="120">Ascenso al cerro morado $120</option>
                                <option value="100">Nocturno con luna llena $100</option>
                                <option value="100">Circuito Amanecer $100</option>
                            </optgroup>
                        </select>
                        <?php echo form_help($ticket->error->amount) ?>
                    </div>
                </div>
		        <div class="control-group <?php echo hasError($ticket->error->date) ?>">
		            <label class="control-label" for="date">
                        Fecha
                    </label>
                    <div class="controls">
                        <input id="date" name="date" class="date" type="text" value="<?php echo $ticket->date ? $ticket->date : date('d-m-Y') ?>">
                        <?php echo form_help($ticket->error->date) ?>
                    </div>
		        </div>
                <p>
		          <button type="submit" class="btn btn-success"><i class="fa fa-credit-card"></i> Seleccionar formas de pago</button>
                </p>
                <hr>
                <p class="muted">Ud. será redireccionado a <a href="http://mercadopago.com"><img src="/assets/img/mp.png"/></a></p>
			  	</fieldset>
			</form>
        </div>
    </div>
</div>