<div class="row">
    <div class="span12">
    	<div class="alert alert-<?php echo $type ?> text-center">
    		<p>
    			<i class="<?php echo $icon ?> fa-5x"></i>
    		</p>
    		<h4>
    			<?php echo $msg ?>
    		</h4>
    	</div>
        <form method="POST" action="/tickets/verify/<?php echo $ticket->id ?>">
            <input type="hidden" value="csfr" name="csfr" />
            <button class="btn input-block-level">
                <i class="fa fa-check"></i> Chequear</button>
        </form>
    </div>
</div>