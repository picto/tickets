<html>
<head>
	<title>Tickets · Impresión de ticket</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
	<style type="text/css" media="all">
		body {
			font-family: monospace;
			padding: 0;
		}
		.left {
			float:left;
			width: 300px;
			padding: 10px 10px;
			height: 205px;
			border: 2px solid #000;
			border-right-style: dashed;
			border-radius:5px 0 0 5px;
		}
		.logo {
			float:left;
			padding-right:10px;
		}
		.right {
			float:left;
			width:205px;
			height:205px;
			padding:10px;
			border: 2px solid #000;
			border-radius: 0 5px 5px 0;
			border-left:0;
		}
		.img {
			float:left;
		}
		.date {
			border: 1px solid #DDD;
			text-align: center;
			font-size: 20px;
			padding: 10px;
			background: #EEE;
			border-radius: 5px;
			color: #000;
		}
		.ref {
			font-size:16px;
			text-transform: uppercase;
		}
		.date {
			font-size:24px;
		}

	</style>
</head>
<body>
	<div class="container">
		<div class="left">
			<div class="logo">
				<img src="/assets/img/logo_sm.png">
			</div>
			<p class="id">#<?php echo $t->id ?> <?php echo $t->created ?></p>
			<p class="ref">#ref <?php echo $t->reference; ?></p>
			<p class="date"><?php echo date_format(date_create($t->date),'d/m/Y');  ?></p>
			<p class="ref">$<?php echo number_format($t->amount,2,',','.'); ?> / <?php echo $t->status ?></p>
			<p><?php echo $t->user_name." ".$t->user_lastname ?></p>
		</div>
		<div class="right">
			<img src="/assets/img/qr/<?php echo $t->id ?>.png" class="thumbnail"/>
		</div>
	</div>
	<div style="clear:both;">
		<br>
		<br>
		<p>
			Mira el mapa si necesitas mayor información sobre cómo llegar.
		</p>
		<p>
			<a href="http://www.ischigualasto.gob.ar/sitio/es/mapa.htm">http://www.ischigualasto.gob.ar/sitio/es/mapa.htm</a>
		</p>
	</div>
</body>
</html>