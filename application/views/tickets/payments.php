<div class="row">
    <div class="span12">
        <div class="bordered">
            <h2 class="page-header"><i class="fa fa-ticket"></i> Pagos</h2>
            <form method="post" class="form-inline">
            	<i class="fa fa-user"></i>
            	<?php echo form_dropdown('user_id',as_dropdown($users,'id','email'),set_value('user_id'),'id="user_id"') ?>
	            <i class="fa fa-filter"></i>
	            <select id="status" name="status">
	            	<option value="0" <?php echo set_select('status','0') ?>>Todos</option>
	            	<option value="approved" <?php echo set_select('status','approved') ?>>Aprobados</option>
	            	<option value="pending" <?php echo set_select('status','pending') ?>>Pendientes</option>
	            	<option value="in_process" <?php echo set_select('status','in_process') ?>>En proceso</option>
	            	<option value="rejected" <?php echo set_select('status','rejected') ?>>Rechazados</option>
	            	<option value="refunded" <?php echo set_select('status','refunded') ?>>Reintegrados</option>
	            	<option value="cancelled" <?php echo set_select('status','cancelled') ?>>Cancelados</option>
	            	<option value="in_mediation" <?php echo set_select('status','in_mediation') ?>>En mediación</option>
	            </select>
                <i class="fa fa-calendar"></i>
                <input name="date" class="date-filter input-small" type="text" value="<?php echo set_value('date') ?>"></input>
                <div class="btn-group">
    	            <button type="submit" class="btn">
    	            	<i class="fa fa-search"></i>
    	           	</button>
                    <button type="submit" class="btn print">
                        <i class="fa fa-print"></i>
                    </button>
                </div>
            </form>
            <?php if($tickets->exists()): ?>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Usuario</th>
                        <th>Monto</th>
                        <th>Fecha evento</th>
                        <th>Creado</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                <?php foreach($tickets as $t): ?>
                    <tr>
                        <td class="pop" title='<i class="fa fa-info"></i> Información de usuario' data-content='<?php echo '<p><i class="fa fa-user"></i> '.$t->user_name.' '.$t->user_lastname.'</p><p><i class="fa fa-phone"></i> '.$t->user_phone.'</p>'.'</p><p><i class="fa fa-envelope-o"></i> '.$t->user_email.'</p>' ?>'>
                        	<?php echo $t->user_email ?>
                        </td>
                        <td>$ <?php echo number_format($t->amount,2,',','.') ?></td>
                        <td><?php echo date_format(date_create($t->date),'d/m/Y') ?></td>
                        <td><?php echo date_format(date_create($t->created),'d/m/Y h:i') ?></td>
                        <td><?php echo status($t->status) ?></td>
                        <td>
                            <div class="btn-group">
                                <?php echo print_btn($t); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach?>
                </table>
                <div class="pagination pagination-centered">
                    <ul>
                        <li class="<?php echo ($page > 1) ? '' : 'disabled' ?>">
                            <a href="<?php echo ($page > 1) ? site_url('tickets/page/'.($page-1)) : '#' ?>">Anterior</a>
                        </li>
                        <li class="disabled">
                            <a href="#"><?php echo $page." de ".ceil($total/$offset) ?></a>
                        </li>
                        <li class="<?php echo (($total/$offset) > $page) ? '' : 'disabled' ?>">
                            <a href="<?php echo (($total/$offset) > $page) ? site_url('tickets/page/'.($page+1)) : '#' ?>">Siguiente</a>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="alert alert-info">
                    <i class="icon-info-sign"></i>
                    No hay tickets registrados.
                </div>
            <?php endif?>
        </div>
    </div>
</div>