<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['protocol']     = 'smtp';
$config['smtp_host']    = '';
$config['smtp_port']    = '25';
$config['smtp_user']    = '';
$config['smtp_pass']    = '';
$config['smtp_timeout'] = '30';
$config['mailtype']     = 'html';
$config['charset']      = 'utf-8';
$config['newline']      = "\r\n";