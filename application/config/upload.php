<?php

$config['upload_path'] = './attachments/';
$config['allowed_types'] = 'gif|jpg|png|ppt|ppx|pdf|doc|docx|xls|wav|mp3';
$config['max_size'] = '10024';