<?php

$lang['required']			= "El campo %s es requerido.";
$lang['isset']				= "El campo %s debe contener un valor.";
$lang['valid_email']		= "El campo %s debe ser una dirección de correo válida.";
$lang['valid_emails']		= "El campo %s debe contener direcciones de correo válidas.";
$lang['valid_url']			= "El campo %s debe contener una URL válida.";
$lang['valid_ip']			= "El campo %s debe contener una IP válida.";
$lang['min_length']			= "El campo %s debe contener al menos %s caracteres.";
$lang['max_length']			= "El campo %s no puede exceder los %s caracteres.";
$lang['exact_length']		= "El campo %s debe contener exactamente %s caracteres.";
$lang['alpha']				= "El campo %s puede contenr únicamente caracteres alfabéticos.";
$lang['alpha_numeric']		= "El campo %s debe ser alfanumérico.";
$lang['alpha_dash']			= "El campo %s puede contener únicamente caracteres alfanuméricos, guiones y guiones bajos.";
$lang['numeric']			= "El campo %s puede contener sólo números.";
$lang['is_numeric']			= "El campo %s puede contener sólo caracteres numéricos.";
$lang['integer']			= "El campo %s puede contener sólo números enteros.";
$lang['regex_match']		= "El campo %s no posee el formato adecuado.";
$lang['matches']			= "El campo %s no coincide con el campo %s.";
$lang['is_unique'] 			= "El campo %s debe ser un valor único.";
$lang['is_natural']			= "El campo %s puede contener sólo valores positivos.";
$lang['is_natural_no_zero']	= "El campo %s debe ser mayor a cero.";
$lang['decimal']			= "El campo %s sólo puede contener decimales.";
$lang['less_than']			= "El campo %s debe ser menor a %s.";
$lang['greater_than']		= "El campo %s debe ser mayor a %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/spanish/form_validation_lang.php */