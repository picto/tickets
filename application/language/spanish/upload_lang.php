<?php

$lang['upload_userfile_not_set'] = "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit'] = "El tamaño del archivo excede los límites configurados para el servidor.";
$lang['upload_file_exceeds_form_limit'] = "El tamaño del archivo excede el límite de este formulario.";
$lang['upload_file_partial'] = "El archivo fue subido parcialmente.";
$lang['upload_no_temp_directory'] = "La carpeta temporal no existe.";
$lang['upload_unable_to_write_file'] = "El archivo no pudo ser escrito en disco.";
$lang['upload_stopped_by_extension'] = "La subida fué detenida.";
$lang['upload_no_file_selected'] = "Seleccione un archivo a subir.";
$lang['upload_invalid_filetype'] = "El tipo de archivo no está permitido.";
$lang['upload_invalid_filesize'] = "El tamaño del archivo excede el tamaño permitido.";
$lang['upload_invalid_dimensions'] = "La imagen a subir excede los límites de ancho ó alto.";
$lang['upload_destination_error'] = "Se encontró un problema al mover el archivo temporal a su ubicación final.";
$lang['upload_no_filepath'] = "La ruta de subida parece no ser válida.";
$lang['upload_no_file_types'] = "No ha especificado ningún tipo de archivo.";
$lang['upload_bad_filename'] = "El archivo ya existe en el servidor.";
$lang['upload_not_writable'] = "La carpeta de subida de destino no posee permisos de escritura.";


/* End of file upload_lang.php */
/* Location: ./system/language/spanish/upload_lang.php */