<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_tickets extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
			),
			'reference' => array(
				'type' => 'VARCHAR',
				'constraint' => '13'
			),
			'amount' => array(
				'type' => 'DECIMAL(19,2)',
			),
			'date' => array(
				'type' => 'DATE',
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '12',
				'default' => 'pending'
			),
			'checked' => array(
				'type' => 'TINYINT',
				'constraint' => '1',
				'default' => 0
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
                'null' => TRUE,
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->add_key('reference');
		$this->dbforge->add_key('user_id');
		$this->dbforge->create_table('tickets');
	}

	public function down()
	{
		$this->dbforge->drop_table('tickets');
	}
}