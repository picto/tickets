<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_users extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'group_id' => array(
				'type' => 'INT',
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'lastname' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'phone' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => FALSE,
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => FALSE,
			),
			'hash' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
            'token' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
                'null' => TRUE,
			)
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->add_key(array('group_id','email'));
		$this->dbforge->create_table('users');

		$data = array(
			array(
			   'group_id' => 1,
			   'name' => 'Admin',
			   'lastname' => 'Istrador',
			   'email' => 'admin@tickets.com',
			   'phone' => '123456',
			   'password' => '656b307922e79dcffe75c59b40da7c8411067478',
               'hash' => '35f7517f5f4e54797edba1552fba91a6',
               'created' => date('Y-m-d H:i:s'),
			   'updated' => date('Y-m-d H:i:s')
			),
			array(
			   'group_id' => 2,
			   'name' => 'Usu',
			   'lastname' => 'Ario',
			   'email' => 'user@tickets.com',
			   'phone' => '123456',
			   'password' => '656b307922e79dcffe75c59b40da7c8411067478',
			   'hash' => '35f7517f5f4e54797edba1552fba91a6',
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
			)
		);

		$this->db->insert_batch('users',$data);
	}

	public function down()
	{
		$this->dbforge->drop_table('users');
	}
}