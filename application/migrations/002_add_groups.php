<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_groups extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
                'null' => TRUE,
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('groups');

		$data = array(
			array(
               'name' => 'Administrador',
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
			array(
               'name' => 'Usuario',
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
		);

		$this->db->insert_batch('groups',$data);
	}

	public function down()
	{
		$this->dbforge->drop_table('groups');
	}
}