<?php

function active($active,$item)
{
	if($active == $item) echo 'class="active"';
}

function hasError($field)
{
    if(!empty($field)) return 'error';
}

function form_help($field)
{
    if(!empty($field)) return '<span class="help-block">'.$field.'</span>';
}

function as_dropdown($obj,$key,$value)
{
    $dd = array();
    $dd[0] = "Todos";
    foreach($obj as $o)
    {
        $dd[$o->{$key}] = $o->{$value};
    }
    return $dd;
}

function cmp_order($a, $b) {
    return $a['order'] - $b['order'];
}

function status($status) {
    switch ($status) {
        case 'approved':
            return '<span class="label label-success">Aprobado</span>';
            break;
        case 'pending':
            return '<span class="label label-warning">Pendiente</span>';
            break;
        case 'in_process':
            return '<span class="label label-warning">En proceso</span>';
            break;
        case 'rejected':
            return '<span class="label label-important">Rechazado</span>';
            break;
        case 'refunded':
            return '<span class="label label-inverse">Reintegrado</span>';
            break;
        case 'cancelled':
            return '<span class="label label-important">Cancelado</span>';
            break;
        case 'in_mediation':
            return '<span class="label label-success">En mediación</span>';
            break;
    }
}

function print_btn($t) {
    switch ($t->status) {
        case 'approved':
            return '<a href="'.site_url("tickets/print/{$t->id}").'" class="btn btn-mini"><i class="fa fa-print"></i> Imprimir</a>';
            break;
        default:
            return '<button class="btn btn-mini disabled" disabled><i class="fa fa-print"></i> Imprimir</button>';
            break;
    }
}