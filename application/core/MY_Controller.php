<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Controller controller
 *
 * Overrides CI_Controller
 *
 * @package     Controllers
 * @author      rcasares
 */

class MY_Controller extends CI_Controller
{
    protected $data;
    protected $view;
    protected $layout;
    protected $admin = FALSE;
    protected $user_id = FALSE;
    protected $connected = FALSE;
    protected $css = array();
    protected $js = array();

    public function __construct()
    {
        parent::__construct();
        // Uncomment for profiling app
        $this->config->load('application');
        if ($this->config->item('app.debug')) {
            $this->output->enable_profiler(TRUE);
        }
        $this->is_connected();
    }

    /**
     * remaps CI requests and loads views automagically!
     *
     * @author rcasares
     */
    public function _remap($method, $arguments)
    {
        if (method_exists($this, $method))
        {
            call_user_func_array(array($this, $method), array_slice($this->uri->rsegments, 2));
        }
        else
        {
            show_404(strtolower(get_class($this)).'/'.$method);
        }
        $this->_load_view();
    }

    /**
     * loads view inside layout based on controller and method name
     *
     * @author rcasres
     */
    private function _load_view()
    {
        // Si no deseamos cargar la vista, no hacer nada.
        if ($this->view === FALSE) { return; }

        // Obtener o establecer automáticamente la vista y layout
        $view = ($this->view !== null) ?
            $this->view . '.php' :
            $this->router->directory . $this->router->class . '/' . $this->router->method . '.php';

        $layout = ($this->layout !== null) ? $this->layout . '.php' : 'layouts/application.php';

        // default load this assets
        $assets = array(
            'assets/css/bootstrap.min.css',
            'assets/css/font-awesome.min.css',
            'assets/css/datepicker.css',
            'assets/css/application.css',
            'assets/css/bootstrap-responsive.min.css',
            'assets/css/select2.css',
            'assets/css/select2-bootstrap.css'
        );
        // default load this scripts
        $scripts = array(
            'assets/js/jquery.js',
            'assets/js/bootstrap.min.js',
            'assets/js/bootstrap-datepicker.js',
            'assets/js/locales/bootstrap-datepicker.es.js',
            'assets/js/select2.min.js',
            'assets/js/select2_locale_es.js',
            'assets/js/application.js'
        );
        // merge default and pushed assets and scripts
        $assets = array_merge($assets,$this->css);
        $scripts = array_merge($scripts,$this->js);

        $this->load->driver('minify');

        $scripts = $this->minify->combine_files($scripts,'js',FALSE);
        $assets  = $this->minify->combine_files($assets,'css',TRUE);
        $this->minify->save_file($scripts, 'assets/js/application.min.js');
        $this->minify->save_file($assets, 'assets/css/application.min.css');

        // Cargar la vista dentro de data
        $this->data['yield'] = $this->load->view($view, $this->data, TRUE);

        // Mostrar layout con vista incluída
        $this->load->view($layout, $this->data);
    }

    /**
     * checks if user is connected
     *
     * @author rcasares
     **/
    function is_connected()
    {
        if($this->session->userdata('group_id') !== FALSE)
        {
            // Class vars
            $this->connected = TRUE;
            $this->user_id = $this->session->userdata('id');

            if($this->session->userdata('group_id') === '1')
            {
                $this->admin = TRUE;
            }
            // View vars
            $this->data['admin'] = $this->admin;
            $this->data['connected'] = TRUE;
        }
        else
        {
            $this->connected = FALSE;
            $this->data['admin'] = FALSE;
            $this->data['connected'] = FALSE;
        }
    }
}