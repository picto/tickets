<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Welcome controller
 *
 * Manages users login, logout and signup
 *
 * @package     Controllers
 * @author      rcasares
 */

class Welcome extends MY_Controller
{
	/**
     * shows main index page
     *
     * @author rcasares
     **/
    public function index() { }

    /**
     * creates a new user with group_id = student
     *
     * @author rcasares
     **/
	public function signup()
	{
		// If already signed in
        if($this->connected) redirect('/');

        $u = new User();
		if ($this->input->post()) {
            // Mass-assign from post
			$u->from_array($_POST,array('email','name','lastname','password','password_confirm','phone'));
			// Non massive assignment for group
            $g = new Group(2);
            // If creation successful redirect to login screen
			if($u->save($g))
			{
				redirect('login');
			}
		}

        $this->data['user'] = $u;
	}

    /**
     * sends email for password recover
     *
     * @author rcasares
     **/
    public function remember_password()
    {
        // If already signed in
        if($this->connected) redirect('/');

        $u = new User();
        if ($this->input->post()) {
            $u->where('email',$this->input->post('email'))->get();
            if($u->exists())
            {
                $u->token = md5(time());
                $u->save();
                $this->load->library('email');
                $this->load->config('email');
                $this->email->from($this->config->item('smtp_user'),'Virtuelle');
                $this->email->to($u->email);
                $this->email->subject('Recuperación de contraseña');
                $data['token'] = $u->token;
                $msg = $this->load->view('templates/remember_password', $data, TRUE);
                $this->email->message($msg);

                if($this->email->send())
                {
                    $this->session->set_flashdata('alert',array(
                      'type' => 'success',
                      'msg'  => '<i class="icon-info-sign"></i> Revise su cuenta de correo para recuperar su contraseña'
                    ));
                }
                else
                {
                    $this->session->set_flashdata('alert',array(
                      'type' => 'error',
                      'msg'  => '<i class="icon-warning-sign"></i> Ocurrió un error al enviar las instrucciones, intente nuevamente más tarde.'
                    ));
                }

                redirect('forgot');
            }
            else $u->error_message('email','La dirección de correo no existe.');
        }

        $this->data['user'] = $u;
    }

    /**
     * checks user hash for password recovery
     *
     * @author rcasares
     **/
    public function check_token($token = FALSE)
    {
        if(!$token OR empty($token))
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> Ocurrió un error al cambiar su contraseña, intente nuevamente.'
            ));
            redirect('');
        }
        $u = new User;
        $u->where('token',$token)->get();
        if($u->exists())
        {
            $this->load->helper('string');
            $password = random_string('alnum',8);
            $u->token = NULL;
            $u->password = $password;
            $u->password_confirm = $password;
            $u->save();
            $this->data['password'] = $password;
        }
        else
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> Ocurrió un error al cambiar su contraseña, intente nuevamente.'
            ));
            redirect('');
        }
    }

    /**
     * allows users to login
     *
     * @author rcasares
     **/
	public function login()
	{
		// If already signed in
        if($this->connected) redirect('/tickets/buy');

        $u = new User();

		if( $this->input->post() )
		{
            $data = array('email' => $_POST['email'], 'password' => $_POST['password'], 'password_confirm' => $_POST['password']);
        	$u->from_array($data,array('email','password','password_confirm'));

        	if( $u->login() )
	        {
	        	$this->session->set_userdata($u->to_array(array('id','group_id','email','name','lastname')));
                if($u->group_id == 1) redirect('/tickets/payments');
	        	if($u->group_id == 2) redirect('/tickets/buy');
	        }
		}

        $this->data['user'] = $u;
	}

    /**
     * destroys all session data and redirect to home
     *
     * @author rcasares
     **/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}