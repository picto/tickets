<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Payments controller
 *
 * Manages users payments
 *
 * @package     Controllers
 * @author      rcasares
 */

class Payments extends MY_Controller
{
    /**
     * constructor
     *
     * @return void
     * @author rcasares
     **/
    public function __construct()
    {
      parent::__construct();
      if(!$this->connected)
      {
          $this->session->set_flashdata('alert',array(
            'type' => 'error',
            'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
          ));
          redirect('/');
      }
    }

    /**
     * success checkout endpoint
     *
     * @return void
     * @author rcasares
     **/
    public function success()
    {
    	$this->session->set_flashdata('alert',array(
          'type' => 'success',
          'msg'  => '<i class="fa fa-warning"></i> El pago fué procesado correctamente, imprima sus tickets desde aquí.'
        ));
        redirect('/tickets');
    }

    /**
     * failure checkout endpoint
     *
     * @return void
     * @author rcasares
     **/
    public function failure()
    {
    	$this->session->set_flashdata('alert',array(
          'type' => 'error',
          'msg'  => '<i class="fa fa-warning"></i> No pudo procesarse el pago, intente nuevamente.'
        ));
        redirect('/tickets/buy');
    }

    /**
     * pending checkout endpoint
     *
     * @return void
     * @author rcasares
     **/
    public function pending()
    {
    	$this->session->set_flashdata('alert',array(
          'type' => 'warning',
          'msg'  => '<i class="fa fa-clock-o"></i> Sus tickets estarán disponibles para imprimir una vez que el pago se haya confirmado.'
        ));
        redirect('/tickets');
    }
}