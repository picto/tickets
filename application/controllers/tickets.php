<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tickets controller
 *
 * Manages users tickets
 *
 * @package     Controllers
 * @author      rcasares
 */

class Tickets extends MY_Controller {
	   
    /**
     * mercadopago instance var
     **/
    var $mp = null;

    /**
     * constructor, instantiates mercadopago SDK
     *
     * @return void
     * @author rcasares
     **/
    public function __construct()
    {
      parent::__construct();
      if(!$this->connected)
      {
          $this->session->set_flashdata('alert',array(
            'type' => 'error',
            'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
          ));
          redirect('/');
      }
      require_once(APPPATH.'third_party/mercadopago/lib/mercadopago.php');
      $this->mp = new MP($this->config->item('mp.id'),$this->config->item('mp.secret'));
    }

    /**
     * list user tickets
     *
     * @return void
     * @author rcasares
     **/
    public function index($page = 1)
    {
    	$tickets = new Ticket;
      $this->data['tickets'] = $tickets->where('user_id',$this->user_id)->order_by('created','desc');
      $this->data['page']    = $page;
      $this->data['offset']  = 10;
      $this->data['users']   = $tickets->get_paged($page,$this->data['offset']);
      $this->data['total']   = $tickets->count();
    }

    /**
     * list user tickets
     *
     * @return void
     * @author rcasares
     **/
    public function payments($page = 1)
    {
      if(!$this->admin)
      {
          $this->session->set_flashdata('alert',array(
            'type' => 'error',
            'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
          ));
          redirect('/');
      }
      $tickets = new Ticket;
      if ($this->input->post())
      {
        if ($this->input->post('user_id') !== '0')
        {
          $tickets->where('user_id',$this->input->post('user_id'));
        }
        if ($this->input->post('status') !== '0')
        {
          $tickets->where('status',$this->input->post('status'));
        }
        if ($this->input->post('date') !== '')
        {
          $date = DateTime::createFromFormat('d-m-Y',$this->input->post('date'))->format('Y-m-d');
          $tickets->where('date',$date);
        }
      }
      $this->load->helper('form');
      $users = new User;
      $this->data['users'] = $users->where('group_id',2)->get_iterated();
      
      $this->data['page']    = $page;
      $this->data['offset']  = 10;
      $this->data['tickets']   = $tickets->include_related('user')->get_paged($page,$this->data['offset']);
      $this->data['total']   = $tickets->count();
    }

    /**
     * imprime la lista filtrada
     *
     * @author rcasares
     **/
    public function print_list()
    {
      if(!$this->admin)
      {
          $this->session->set_flashdata('alert',array(
            'type' => 'error',
            'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
          ));
          redirect('/');
      }
      $tickets = new Ticket;
      if ($this->input->post())
      {
        if ($this->input->post('user_id') !== '0')
        {
          $tickets->where('user_id',$this->input->post('user_id'));
        }
        if ($this->input->post('status') !== '0')
        {
          $tickets->where('status',$this->input->post('status'));
        }
        if ($this->input->post('date') !== '')
        {
          $date = DateTime::createFromFormat('d-m-Y',$this->input->post('date'))->format('Y-m-d');
          $tickets->where('date',$date);
        }
      }
      $this->data['tickets']   = $tickets->include_related('user')->get_iterated();
      $this->view = FALSE;
      $this->load->view('tickets/print_list',array('tickets' => $tickets));
    }

    /**
     * print user tickets
     *
     * @return void
     * @author rcasares
     **/
    public function print_ticket($id)
    {
        $t = new Ticket($id);
        if($t->status !== 'approved' OR $t->user_id != $this->user_id)
        {
          if(!$this->admin)
          {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> El ticket no puede ser impreso debido a que no ha sido aprobada su compra.'
            ));
            redirect('tickets');
          }
        }

        $qr = FCPATH.'/assets/img/qr/'.$t->id.'.png';
        if(!file_exists($qr)) {
            $this->load->library('ciqrcode');
            $params['data'] = site_url('tickets/verify/'.$t->id);
            $params['level'] = 'H';
            $params['size'] = 4;
            $params['savename'] = $qr;
            $this->ciqrcode->generate($params);
        }

        $this->view = FALSE;
        $this->load->view('tickets/print_ticket',array('t' => $t));
    }

    /**
     * comprueba que el ticket sea valido y no haya sido utilizado
     *
     * @author rcasares
     **/
    public function verify($id)
    {   
        if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('/login');
        }
        $t = new Ticket($id);

        if(!$t->exists() OR ($t->status != 'approved')) {
            $msg = "El ticket es inválido";
            $type = "error";
            $icon = "fa fa-warning";
        }
        if($t->exists() AND $t->checked) {
            $msg = "El ticket ya fue chequeado";
            $type = "error";
            $icon = "fa fa-warning";
        }
        // TODO: Validar si la fecha del ticket se corresponde con fecha actual?
        if($t->exists() AND !$t->checked AND ($t->status == 'approved')) {
            $msg = "El ticket es válido";
            $icon = "fa fa-check";
            $type = "success";
        }

        if ($this->input->post() AND $t->exists() AND !$t->checked)
        {
            $t->checked = 1;
            $t->save();
            $msg = "El ticket fue chequeado correctamente";
            $icon = "fa fa-check";
            $type = "success";
        }

        // set vars available to view
        $this->data['ticket'] = $t;
        $this->data['msg'] = $msg;
        $this->data['type'] = $type;
        $this->data['icon'] = $icon;
    }

    /**
     * comprar un nuevo ticket
     *
     * @author rcasares
     **/
    public function buy()
    {
      $t = new Ticket;
      if($this->input->post())
      {   
          // data for insert
          $ticket = array(
            'user_id' => $this->user_id,
            'date' => $this->input->post('date'),
            'amount' => (int) $this->input->post('amount'),
            'reference' => uniqid()
          );
          // populate object
          $t->from_array($ticket,array('user_id','date','amount','reference'));
          // save
          if($t->save())
          {
            $preference = array(
              "external_reference" => $t->reference,
              "items" => array(
                array(
                  "title"       => "Ticket",
                  "quantity"    => 1,
                  "currency_id" => "ARS",
                  "unit_price"  => $t->amount
                )
              ),
              "back_urls" => array(
                  "success" => $this->config->item('url.success'),
                  "failure" => $this->config->item('url.failure'),
                  "pending" => $this->config->item('url.pending')
              )
            );

            try {
              $res = $this->mp->create_preference($preference);
              redirect($res['response']['sandbox_init_point']);
            } catch (Exception $e) {
              $this->session->set_flashdata('alert',array(
                'type' => 'error',
                'msg'  => '<i class="icon-warning-sign"></i> '.$e->getMessage()
              ));
              redirect('tickets/buy');
            }
          }
      }

      $this->data['ticket'] = $t;
    }
}