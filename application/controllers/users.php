<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Users controller
 *
 * Manages users password change and profile
 *
 * @package     Controllers
 * @author      rcasares
 */

class Users extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->connected)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('/');
        }
    }

    /**
     * lists users
     *
     * @author rcasares
     **/
    public function index($page = 1)
    {
        if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('/');
        }

        $u = new User;
        $this->data['page'] = $page;
        $this->data['offset'] = 10;
        $this->data['users'] = $u->include_related('group')->get_paged($page,$this->data['offset']);
        $this->data['total'] = $u->count();
    }

    /**
     * changes user password
     *
     * @author rcasares
     **/
    public function change_password()
    {
        $u = new User($this->user_id);

        if ($this->input->post())
        {
            $data = array(
                'email' => $u->email,
                'password' => $_POST['current_password'],
                'password_confirm' => $_POST['current_password']
            );
            $u->from_array($data,array('email','password','password_confirm'));

            if($u->login())
            {
                $u = new User($this->user_id);

                $u->password = $this->input->post('password');
                $u->password_confirm = $this->input->post('password_confirm');
                if($u->save())
                {
                    $this->session->set_flashdata('alert',array(
                      'type' => 'success',
                      'msg'  => '<i class="icon-info-sign"></i> Su constraseña se cambió correctamente'
                    ));
                    redirect('/');
                }
            }
        }
        $this->data['user'] = $u;
    }

    /**
     * updates user profile
     *
     * @author rcasares
     **/
    public function update_profile($id = FALSE)
    {
        if($this->admin AND $id)
        {
            $u = new User($id);
        }
        else
        {
            $u = new User($this->user_id);
        }

        if ($this->input->post()) {
            // mass-assign from post
            if($this->admin)
            {
                $u->from_array($_POST,array('group_id','email','name','lastname','phone'));
            }
            else
            {
                $u->from_array($_POST,array('email','name','lastname','phone'));
            }

            // if creation successful redirect to browse
            if($u->save())
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'success',
                  'msg'  => '<i class="icon-info-sign"></i> El perfil se actualizó correctamente'
                ));
                if($this->admin) redirect('users');
                else redirect('/');
            }
        }
        $g = new Group;
        $g->get_iterated();
        $this->load->helper('form');
        $this->data['groups'] = as_dropdown($g,'id','name');
        $this->data['user'] = $u;
    }

    /**
     * deletes a user
     *
     * @author rcasares
     **/
    public function delete($id)
    {
        if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }

        if($this->input->post())
        {
            $u = new User;
            $u->where('id',$id)->get()->delete();

            $this->session->set_flashdata('alert',array(
              'type' => 'success',
              'msg'  => '<i class="icon-info-sign"></i> El usuario se eliminó con éxito'
            ));
            redirect("users");
        }
    }
}