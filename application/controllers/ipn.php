<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * IPN controller
 *
 * Manages mercadopago IPN notifications
 *
 * @package     Controllers
 * @author      rcasares
 */

class IPN extends MY_Controller {
	
    /**
     * mercadopago instance var
     *
     **/
    var $mp;

    /**
     * constructor
     *
     * @return void
     * @author rcasares
     **/
    public function IPN()
    {
        parent::__construct();
        // load and mercadopago lib instance
        require_once(APPPATH.'third_party/mercadopago/lib/mercadopago.php');
        $this->mp = new MP($this->config->item('mp.id'),$this->config->item('mp.secret'));
        $this->mp->sandbox_mode($this->config->item('mp.sandbox'));
    }

	/**
     * mercadopago instant payment notifications endpoint
     *
     * @return void
     * @author rcasares
     **/
    public function receive()
    {
        // retrieve payment data
        $p = $this->mp->get_payment($_GET['id']);
        // update ticket db
        $t = new Ticket;
        $t->where('reference',$p['response']['collection']['external_reference']);
        $t->update('status',$p['response']['collection']['status']);
    	$this->view = FALSE;
    }
}